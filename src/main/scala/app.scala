package hiveviewer

import org.scalajs.dom.{ document, XMLHttpRequest, window }
import org.scalajs.dom.svg
import slinky.web.ReactDOM

import sabaki.gametreeFacade.GameTree
import hiveviewer.import_export.SimpleMoveExtractor

import scala.concurrent.ExecutionContext
import org.scalajs.dom.experimental.URLSearchParams

object App {

  implicit val ec = ExecutionContext.global

  // lazy val queryString = new URLSearchParams(window.location.search)

  // def queryStringLookup(name: String): Option[String] =
  //   if(queryString.has(name)) Some(queryString.get(name)) else None

  // def readGameLogFromUrl: Option[GameLog] =
  //   queryStringLookup("g").flatMap { g =>
  //     import_export.SimpleStringExport.doImport(atob(g)) match {
  //       case Right(log) => Some(log)
  //       case Left(err) =>
  //         println(s"failed to import: $err")
  //         None
  //     }
  //   }

  def loadGame(url: String): Either[String, GameTree[Int]] = {
    val xhr = new XMLHttpRequest()
    xhr.open("GET", url, false)
    xhr.send(null)
    if(xhr.status == 200)
      Right(GameTree.fromSgf(xhr.responseText))
    else
      Left(s"xhr request for '$url' failed: ${xhr.status} ${xhr.statusText}")
  }

  def main(args: Array[String]): Unit = {
    val config = Config.fromCookie().getOrElse(Config())

    val canvas = document.getElementById("canvas").asInstanceOf[svg.SVG]
    val localDb = new hiveviewer.database.PouchGameDB(config.localDbName)

    localDb.init()

    // val gameTreeF = config.currentGameName.map { gameName =>
    //   println(s"Loading game: ${gameName}")
    //   localDb.loadGame(gameName).map {
    //     case Left(err) =>
    //       println(s"Error loading game $gameName : $err")
    //       new GameTree[Int]()
    //     case Right(tree) => tree
    //   }
    // }.getOrElse(Future.successful(new GameTree[Int]()))

    val qs = new URLSearchParams(window.location.search)

    val gameTree =
      if(qs.has("g")) {
        val gameFile = qs.get("g")
        // let's load a game file from and XHR
        loadGame(s"sgf/$gameFile") match {
          case Right(tree) => tree.asInstanceOf[GameTree[Int]]
          case Left(err) =>
            window.alert(s"Error: $err")
            new GameTree[Int]()
        }
      } else {
        config.viewerState.rootNode.map(GameTree.fromRootNode _)
          .getOrElse(new GameTree[Int])
      }

//     gameTreeF.onComplete {
//       case Success(gameTree) =>
// //        println(s"Loaded game: ${JSON.stringify(gameTree.root)}")

    val viewer = components.GameViewer(gameTree, SimpleMoveExtractor, config, localDb)
    ReactDOM.render(viewer, canvas)
//       case Failure(err) =>
//         println(s"Failed to load game: $err")
//     }
  }
}
