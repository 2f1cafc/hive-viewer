package hiveviewer.database

import sabaki.gametreeFacade.GameTree
import pouchdb.PouchDB
import scala.concurrent.{ ExecutionContext, Future }
import scala.scalajs.js
import sabaki.gametreeFacade.NodeObject
import pouchdb.DbInfo
import scala.util.Success

trait GameDB {
  def updateGame(id: String, tree: GameTree[Int], rev: Option[String])(implicit ec: ExecutionContext): Future[Either[String, js.Dynamic]]
  def loadGame(id: String)(implicit ec: ExecutionContext): Future[Either[String, (String, GameTree[Int])]]
  def listGames(initial: String)(implicit ec: ExecutionContext): Future[Either[String, List[String]]]
  def deleteGame(id: String, rev: String)(implicit ec: ExecutionContext): Future[Either[String, Unit]]
}

class PouchGameDB(
  private val db: PouchDB,
) extends GameDB {

  def this(dbName: String) = this(new PouchDB(dbName))

  def init()(implicit ec: ExecutionContext): Future[DbInfo] =
    db.info().andThen {
      case Success(info) =>
        println(s"db ${info.db_name} ready, docs: ${info.doc_count}")
    }

  def syncWith(remoteDb: PouchDB)(implicit ec: ExecutionContext): Future[Either[String, pouchdb.EventEmitter]] =
    db.sync(remoteDb).map(_.left.map(_.message))

  def updateGame(id: String, tree: GameTree[Int], rev: Option[String])(implicit ec: ExecutionContext): Future[Either[String,scala.scalajs.js.Dynamic]] = {
    val doc = js.Dynamic.literal("_id" -> id, "rootNode" -> tree.root)
    rev.foreach(r => doc._rev = r)
    db.put(doc).map { res => res.left.map(_.message) }
  }

  def loadGame(id: String)(implicit ec: ExecutionContext): Future[Either[String, (String, GameTree[Int])]] =
    db.get(id).map {
      case Left(err) => Left(err.message)
      case Right(doc) =>
        val rootNode = doc.rootNode.asInstanceOf[NodeObject[Int]]
        Right((doc._rev.asInstanceOf[String], GameTree.fromRootNode(rootNode)))
    }

  def listGames(initial: String)(implicit ec: ExecutionContext):
      Future[Either[String, List[String]]] = {
    db.allDocs().map { res =>
      res.map { allDocsRes =>
        allDocsRes.rows.toList.map(_.id)
      }.left.map(_.message)
    }
  }

  def deleteGame(id: String, rev: String)(implicit ec: ExecutionContext):
      Future[Either[String,Unit]] =
    db.remove(id, rev).map { res =>
        res.map(_ => ()).left.map(_.message)
      }
}
