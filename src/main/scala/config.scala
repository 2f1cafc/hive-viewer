package hiveviewer

import scala.scalajs.js
import org.scalajs.dom
import sabaki.gametreeFacade.NodeObject

import slinky.readwrite.{ Reader, Writer }

case class Config(
  localDbName: String = "hiveviewer",
  remoteDbName: Option[String] = None,
  remoteDbUser: Option[String] = None,
  remoteDbPass: Option[String] = None,
  viewerState : GameViewerState = GameViewerState()
) {
  def modifyViewerState(f: GameViewerState => GameViewerState): Config = copy(viewerState = f(viewerState))
}

case class GameViewerState(
  currentGameName: Option[String] = None,
  currentNodeId: Option[Int] = None,
  currentPath: js.Dictionary[Int] = js.Dictionary.empty,
  zoomLevel: Double = 1.0,
  docRevision: Option[String] = None,
  rootNode: Option[NodeObject[Int]] = None,
) {
  def isValid: Boolean = currentNodeId.isEmpty || rootNode.isDefined
}


object Config {

  val localStorage = dom.window.localStorage

  def toJson(cfg: Config) = implicitly[Writer[Config]].write(cfg)

  def fromJson(json: js.Object) = implicitly[Reader[Config]].read(json)

  def fromCookie(localStorageKey: String = "hive-viewer.config"): Option[Config] =
    Option(localStorage.getItem(localStorageKey)).map { jsonStr =>
      val json = js.JSON.parse(jsonStr).asInstanceOf[js.Object]
      val cfg = fromJson(json)
      if(cfg.viewerState.isValid) cfg else {
        println(s"Ignoring invalid viewerState: ${jsonStr}")
        cfg.copy(viewerState = GameViewerState())
      }
    }

  def toCookie(config: Config, localStorageKey: String = "hive-viewer.config"): Unit = {
    val configStr = js.JSON.stringify(toJson(config))
    localStorage.setItem(localStorageKey, configStr)
  }
}
