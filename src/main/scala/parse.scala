package hiveviewer

// simple parser for the move instructions

import hiveviewer.model._

object MoveParser {
  import fastparse._, NoWhitespace._

  object ps {
    def colour[_ : P] = P(CharIn("bw").!.map {
      case "b" => Colours.Black
      case "w" => Colours.White
    })
    def insect[_ : P] = P(CharPred(c => Insects.lookup.isDefinedAt(c.toString)).!.map(Insects.lookup))
    def number[_ : P] = P(CharIn("1-3").!.? map {
      case Some(n) => n.toInt
      case None => 1
    })

    def piece[_ : P] = P((colour ~ insect ~ number) map {
      case (c, i, n) => PieceId(c, i, n)
    })

    def side[_ : P] = P(CharIn("\\\\/\\-").!) // blackslash needs a double escape

    def leftSide[_ : P] = P((side ~ piece).flatMap {
      case ("\\", p) => Pass(Location(p, Sides.NW))
      case ("/", p)  => Pass(Location(p, Sides.SW))
      case ("-", p)  => Pass(Location(p, Sides.W))
      case (_, _) => Fail
    })

    def rightSide[_ : P] = P((piece ~ side).flatMap {
      case (p, "\\") => Pass(Location(p, Sides.SE))
      case (p, "/")  => Pass(Location(p, Sides.NE))
      case (p, "-")  => Pass(Location(p, Sides.E))
      case (_, _) => Fail
    })

    def atopSide[_ : P] = P(piece.map(p => Location(p, Sides.Atop)))

    def location[_ : P] = P(rightSide | leftSide | atopSide)

    def move[_ : P] = P((piece ~ (CharIn(" \t").rep(1) ~ location).? ~ End).map { // parse all the input for a valid move
      case (p, loc) => Move(p, loc)
    })
  }

  def pieceFromString(s: String) = parse(s, ps.piece(_)).get.value

  def apply(s: String): Either[String, Move] = parse(s, ps.move(_)) match {
    case Parsed.Success(move, _) => Right(move)
    case Parsed.Failure(msg, _, _) => Left(s"failed to parse: [$s] ($msg)")
  }

}
