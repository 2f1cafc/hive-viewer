package hiveviewer.util

import scala.scalajs.js
import scala.scalajs.js.annotation.JSImport

/* scalajs facade for: https://github.com/js-cookie/js-cookie */

trait CookieSetOpts extends js.Object {
  val expires: js.UndefOr[Int] = js.undefined
  val path: js.UndefOr[String] = js.undefined
  val sameSite: js.UndefOr[String] = js.undefined
}

//  import Cookies from '/path/to/js.cookie.mjs'
@js.native
@JSImport("js-cookie", JSImport.Default)
object Cookies extends js.Object {
  def get(name: String): js.UndefOr[String] = js.native
  def get(): js.Dictionary[js.Any] = js.native
  def remove(name: String): Unit = js.native

  def set(name: String, value: String,
    opts: js.UndefOr[CookieSetOpts] = js.undefined): Unit = js.native
}
