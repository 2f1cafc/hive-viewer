package hiveviewer

package object model {
  type BoardMap = Map[PieceId, PiecePos]
}
