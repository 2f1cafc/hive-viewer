package hiveviewer.model

sealed abstract class Insect(val name: String, val isSingleton: Boolean = false)
object Insects {
  case object Ant         extends Insect("A")
  case object Grasshopper extends Insect("G")
  case object Beetle      extends Insect("B")
  case object Spider      extends Insect("S")
  case object Ladybug     extends Insect("L", true)
  case object Mosquito    extends Insect("M", true)
  case object Pillbug     extends Insect("P", true)
  case object Queen       extends Insect("Q", true)

  val all: List[Insect] = List(Ant, Grasshopper, Beetle, Ladybug, Spider, Mosquito, Pillbug, Queen)
  val allNames = all.map(_.name)
  val lookup = all.map(i => i.name -> i).toMap
}

case class PieceId(colour: Colour, insect: Insect, number: Int) {
  override def toString = {
    val colourStr = colour match {
      case Colours.Black => "b"; case Colours.White => "w"
    }
    val numberStr = // there is only one queen on each side so number isn't neccessary
      if(insect.isSingleton) "" else number.toString

    s"$colourStr${insect.name}$numberStr"
  }
}

sealed trait Colour { val name: String }

object Colours {
  case object Black extends Colour { val name = "black" }
  case object White extends Colour { val name = "white" }
}

// defines the hex space as a 3-dimensional coordinate system
//     -z    /\    +y
//          /  \
//     -x  |    |  +x
//         |    |
//          \  /
//     -y    \/    +z

sealed trait PiecePos
case class BarLoc(board: Int, col: Int, height: Int) extends PiecePos
case class HexLoc(x: Double, y: Double, z: Double, h: Int = 0) extends PiecePos {
  def `+`(other: HexLoc) = HexLoc(x = x + other.x, y = y + other.y, z = z + other.z, h = h + other.h)
}

abstract sealed class Side(val hexDelta: HexLoc)
object Sides {
  case object NW   extends Side(HexLoc(x = 0,  y = 0,  z = -1, h = 0))
  case object NE   extends Side(HexLoc(x = 0,  y = 1,  z = 0, h = 0))
  case object SE   extends Side(HexLoc(x = 0,  y = 0,  z = 1, h = 0))
  case object SW   extends Side(HexLoc(x = 0,  y = -1, z = 0, h = 0))
  case object E    extends Side(HexLoc(x = 1,  y = 0,  z = 0, h = 0))
  case object W    extends Side(HexLoc(x = -1, y = 0,  z = 0, h = 0))
  case object Atop extends Side(HexLoc(x =  0, y = 0,  z = 0, h = 1))

  def invert(side: Side): Side = side match {
    case NW   => SE
    case NE   => SW
    case SE   => NW
    case SW   => NE
    case E    => W
    case W    => E
    case Atop => Atop
  }
}

case class Location(targetPiece: PieceId, side: Side) {
  override def toString = side match {
    case Sides.NW => "\\" + targetPiece.toString
    case Sides.SW => "/"  + targetPiece.toString
    case Sides.W  => "-" + targetPiece.toString
    case Sides.NE => targetPiece.toString + "/"
    case Sides.SE => targetPiece.toString + "\\"
    case Sides.E  => targetPiece.toString + "-"
    case Sides.Atop => targetPiece.toString
  }
}

case class Move(
  piece: PieceId,
  location: Option[Location], /* none means initial move */
  comment: Option[String] = None
) {
  override def toString = piece.toString + (location match {
    case Some(l) => " " + l.toString
    case None => ""
  })
}

// sealed trait GameLogEntry

// case class Move(piece: PieceId, location: Option[Location], /* none means initial move */
//   comment: Option[String] = None) {
//   override def toString = piece.toString + (location match {
//     case Some(l) => " " + l.toString
//     case None => ""
//   })
// }

// case class GameLog(moves: List[GameLogEntry] = Nil) {
//   def updatedEntry(entry: GameLogEntry, idx: Int): GameLog = copy(moves = moves.updated(idx, entry))
// }
