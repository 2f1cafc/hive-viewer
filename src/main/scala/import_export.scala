package hiveviewer.import_export

import hiveviewer.model._
import hiveviewer.MoveParser
import sabaki.gametreeFacade.GameTree

// import hiveviewer.model._
// import scala.scalajs.js

// trait Importer {
//   def doImport(source: String): Either[String, GameLog]
// }

// trait Exporter[T] {
//   def doExport(input: GameLog): Either[String, T]
// }

// object JSONExport extends Exporter[String] with Importer {

//   import js.JSConverters._

//   def convertMove(m: Move) =
//     /* these field names are intentionally short */
//     js.Dynamic.literal(p = m.piece.toString,
//       l = m.location.map(_.toString).orUndefined,
//       c = m.comment.orUndefined)

//   def doExport(input: GameLog): Either[String, String] = {
//     val movesArr = input.moves.collect {
//       case m: Move => convertMove(m)
//     }.toJSArray
//     val encoded = js.JSON.stringify(js.Dynamic.literal(m = movesArr))
//     Right(encoded)
//   }

//   def doImport(input: String) = Left("unimplemented")
// }

// object SimpleStringExport extends Exporter[String] with Importer {
//   def doExport(input: GameLog): Either[String, String] =
//     Right(
//       input.moves.collect {
//         case m: Move => m.toString
//       }.mkString(";")
//     )

//   def doImport(input: String) = {
//     val maybes = input.split(";").map(hiveviewer.MoveParser.apply)
//     val (success, fails) = maybes.partition(_.isRight)
//     if(fails.nonEmpty) {
//       Left(fails.map(_.left.get).mkString("; "))
//     } else {
//       Right(GameLog(success.map(_.right.get).toList))
//     }
//   }
// }

trait MoveExtractor {
  // TODO: ? should the move extractor return a list of moves instead?
  def extractMove(prev: BoardMap, nodeId: Int, tree: GameTree[Int]): BoardMap

  def extractText(nodeId: Int, tree: GameTree[Int]): Option[String]

  def isValidMove(s: String): Boolean
}

object SimpleMoveExtractor extends MoveExtractor {

  def extractMove(prev: BoardMap, nodeId: Int, tree: GameTree[Int]): BoardMap =
    extractText(nodeId, tree).flatMap { moveText =>
      MoveParser(moveText) match {
        case Right(move) => Some(executeMove(move, prev))
        case Left(err) =>
          println(s"parsing error: $err ($moveText)")
          None
      }
    }.getOrElse(prev)

  def executeMove(move: Move, cur: BoardMap) = move match {
    case Move(movingPiece, None, _) => putPieceInitial(movingPiece, cur)
    case Move(movingPiece, Some(loc), _)  => putPieceBy(movingPiece, loc, cur)
  }

  def putPieceBy(newPieceId: PieceId, loc: Location, cur: BoardMap) = {
    cur(loc.targetPiece) match {
      case targetPieceHex: HexLoc =>
        putPieceAtHex(newPieceId, targetPieceHex + loc.side.hexDelta, cur)
      case _ =>
        // TODO: handle error of piece not yet placed = invalid move
        cur
    }
  }

  def putPieceInitial(pieceId: PieceId, cur: BoardMap) =
    putPieceAtHex(pieceId, HexLoc(0, 0, 0), cur)

  def putPieceAtHex(newPieceId: PieceId, hexLoc: HexLoc, cur: BoardMap) =
    cur.updated(newPieceId, hexLoc)

  def extractText(nodeId: Int, tree: GameTree[Int]): Option[String] = {
    val node = tree.get(nodeId)
    node.prop1[String]("W") orElse node.prop1[String]("B")
  }

  def isValidMove(s: String): Boolean = MoveParser(s).isRight
}
