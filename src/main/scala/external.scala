/* external components ScalaJS-ified (https://slinky.dev/docs/external-components/) */

package hiveviewer.components

import org.scalajs.dom

import scalajs.js
import scala.scalajs.js.annotation.JSImport
import slinky.core._
import slinky.core.annotations.react
import slinky.web.html._

@JSImport("react-inline-editing", JSImport.Default)
@js.native
object EditableLabelJS extends js.Object {
  val EditableComponent: js.Object = js.native
}

@react object EditableLabel extends ExternalComponent {
  case class Props(
    text: String,
    isEditing: Boolean = false,
    emptyEdit: Boolean = false,
    labelClassName: Option[String] = None,
    labelPlaceHolder: Option[String] = None,
    onFocusOut: String => Unit = _ => ()
  )
  val component = EditableLabelJS
}

// https://react-bootstrap.github.io/layout/grid/

object ReactBootstrap {
  @JSImport("react-bootstrap/Container", JSImport.Default)
  @js.native
  object Container extends js.Object

  @JSImport("react-bootstrap/Row", JSImport.Default)
  @js.native
  object Row extends js.Object

  @JSImport("react-bootstrap/Col", JSImport.Default)
  @js.native
  object Col extends js.Object

  @JSImport("react-bootstrap/Card", JSImport.Default)
  @js.native
  object Card extends js.Object {
    val Header: js.Object = js.native
    val Title: js.Object = js.native
    val Body: js.Object = js.native
    val Text: js.Object = js.native
  }


  @JSImport("react-bootstrap/Collapse", JSImport.Default)
  @js.native
  object Collapse extends js.Object

  @JSImport("react-bootstrap/ListGroup", JSImport.Default)
  @js.native
  object ListGroup extends js.Object {
    val Item: js.Object = js.native
  }

  @JSImport("react-bootstrap/ButtonToolbar", JSImport.Default)
  @js.native
  object ButtonToolbar extends js.Object

  @JSImport("react-bootstrap/ButtonGroup", JSImport.Default)
  @js.native
  object ButtonGroup extends js.Object

  @JSImport("react-bootstrap/Button", JSImport.Default)
  @js.native
  object Button extends js.Object

  @JSImport("react-bootstrap/Form", JSImport.Default)
  @js.native
  object Form extends js.Object {
    val Group: js.Object = js.native
    val Label: js.Object = js.native
    val Control: js.Object = js.native
    val Text: js.Object = js.native
    val File: js.Object = js.native
  }

  @JSImport("react-bootstrap/DropdownButton", JSImport.Default)
  @js.native
  object DropdownButton extends js.Object

  @JSImport("react-bootstrap/Dropdown", JSImport.Default)
  @js.native
  object Dropdown extends js.Object {
    val Toggle: js.Object = js.native
    val Menu: js.Object = js.native
    val Item: js.Object = js.native
  }

  @JSImport("react-bootstrap/Modal", JSImport.Default)
  @js.native
  object Modal extends js.Object {
    val Dialog: js.Object = js.native
    val Header: js.Object = js.native
    val Title: js.Object = js.native
    val Body: js.Object = js.native
    val Footer: js.Object = js.native
  }

  @JSImport("react-bootstrap/Nav", JSImport.Default)
  @js.native
  object Nav extends js.Object {
    val Item: js.Object = js.native
    val Link: js.Object = js.native
  }

}

@react object Nav extends ExternalComponent {
  type OnSelectCb =
    (String, SyntheticEvent[dom.Element, dom.Event]) => Unit

  val noop: OnSelectCb = (_, _) => ()

  case class Props(
    variant: String = "",
    onSelect: OnSelectCb = noop,
    activeKey: Option[String] = None)

  val component = ReactBootstrap.Nav

  object Item extends ExternalComponentNoProps {
    val component = ReactBootstrap.Nav.Item
  }
  @react object Link extends ExternalComponent {
    case class Props(eventKey: String)
    val component = ReactBootstrap.Nav.Link
  }
}

@react object Modal extends ExternalComponent {
  case class Props(show: Boolean, onHide: () => Unit = () => ())

  val component = ReactBootstrap.Modal

  object Dialog extends ExternalComponentNoProps {
    val component = ReactBootstrap.Modal.Dialog
  }
  @react object Header extends ExternalComponent {
    case class Props(closeButton: Boolean = true)

    val component = ReactBootstrap.Modal.Header
  }
  object Title extends ExternalComponentNoProps {
    val component = ReactBootstrap.Modal.Title
  }
  object Body extends ExternalComponentNoProps {
    val component = ReactBootstrap.Modal.Body
  }
  object Footer extends ExternalComponentNoProps {
    val component = ReactBootstrap.Modal.Footer
  }
}

// this doesn't work yet
object DropdownButton extends
    ExternalComponentNoPropsWithAttributes[button.tag.type] {
  val component = ReactBootstrap.DropdownButton
}

object Dropdown extends ExternalComponentNoProps {
  override val component = ReactBootstrap.Dropdown

  object Toggle extends ExternalComponentNoPropsWithAttributes[button.tag.type] {
    override val component = ReactBootstrap.Dropdown.Toggle
  }

  object Menu extends ExternalComponentNoProps {
    override val component = ReactBootstrap.Dropdown.Menu
  }

  object Item extends ExternalComponentNoPropsWithAttributes[button.tag.type] {
    override val component = ReactBootstrap.Dropdown.Item
  }
}

object Card extends ExternalComponentNoPropsWithAttributes[div.tag.type] {

  override val component = ReactBootstrap.Card

  object Header extends ExternalComponentNoPropsWithAttributes[div.tag.type] {
    override val component = ReactBootstrap.Card.Header
  }

  object Title extends ExternalComponentNoPropsWithAttributes[div.tag.type] {
    override val component = ReactBootstrap.Card.Title
  }

  object Body extends ExternalComponentNoPropsWithAttributes[div.tag.type] {
    override val component = ReactBootstrap.Card.Body
  }

  object Text extends ExternalComponentNoPropsWithAttributes[div.tag.type] {
    override val component = ReactBootstrap.Card.Text
  }
}

object Form extends ExternalComponentNoPropsWithAttributes[form.tag.type] {
  override val component = ReactBootstrap.Form

  object attr {
    val label = CustomAttribute[String]("label")
    val as = CustomAttribute[String]("as")
    val rows = CustomAttribute[Int]("rows")
  }

  object Group extends ExternalComponentNoProps {
    override val component = ReactBootstrap.Form.Group
  }

  object Label extends ExternalComponentNoProps {
    override val component = ReactBootstrap.Form.Label
  }

  object Control extends ExternalComponentNoPropsWithAttributes[input.tag.type] {
    override val component = ReactBootstrap.Form.Control
  }

  object Text extends ExternalComponentNoPropsWithAttributes[div.tag.type] {
    override val component = ReactBootstrap.Form.Text
  }

  object File extends ExternalComponentNoPropsWithAttributes[input.tag.type] {
    override val component = ReactBootstrap.Form.File
  }
}

@react object Button extends ExternalComponentWithAttributes[button.tag.type] {
  case class Props(disabled: Boolean = true)

  val size = CustomAttribute[String]("size")
  val variant = CustomAttribute[String]("variant")
  val href = CustomAttribute[String]("href") // this turns a button into an anchor
  val as = CustomAttribute[String]("as")
  val `type` = CustomAttribute[String]("type")

  override val component = ReactBootstrap.Button
}

object ButtonGroup extends ExternalComponentNoPropsWithAttributes[div.tag.type] {
  override val component = ReactBootstrap.ButtonGroup
}

object ButtonToolbar extends ExternalComponentNoPropsWithAttributes[div.tag.type] {
  override val component = ReactBootstrap.ButtonToolbar
}

object ListGroup extends ExternalComponentNoPropsWithAttributes[div.tag.type] {
  override val component = ReactBootstrap.ListGroup
  object Item extends ExternalComponentNoPropsWithAttributes[div.tag.type] {
    override val component = ReactBootstrap.ListGroup.Item
  }

  val active = CustomAttribute[Boolean]("active")
  val action = CustomAttribute[Boolean]("action")
}

@react object Container extends ExternalComponent {
  type Props = Unit
  val component = ReactBootstrap.Container
}

@react object Row extends ExternalComponent {
  type Props = Unit
  val component = ReactBootstrap.Row
}

@react object Collapse extends ExternalComponent {
  case class Props(in: Boolean)
  val component = ReactBootstrap.Collapse
}

@react object Col extends ExternalComponent {
  case class Props(xs: Option[String] = None)
  val component = ReactBootstrap.Col

  val xs = CustomAttribute[Int]("xs")
  val md = CustomAttribute[Int]("md")
}

object OcticonsJS {
  @JSImport("@primer/octicons-react", JSImport.Default)
  @js.native
  object Octicon extends js.Object

  @JSImport("@primer/octicons-react", "Database")
  @js.native
  object Database extends js.Object

  @JSImport("@primer/octicons-react", "Settings")
  @js.native
  object Settings extends js.Object

  @JSImport("@primer/octicons-react", "Archive")
  @js.native
  object Archive extends js.Object

  @JSImport("@primer/octicons-react", "Trashcan")
  @js.native
  object Trashcan extends js.Object

  @JSImport("@primer/octicons-react", "ArrowLeft")
  @js.native
  object ArrowLeft extends js.Object

  @JSImport("@primer/octicons-react", "ArrowRight")
  @js.native
  object ArrowRight extends js.Object

}

object Octicon extends ExternalComponentNoProps {
  val component = OcticonsJS.Octicon

  object ArrowLeft extends ExternalComponentNoProps {
    val component = OcticonsJS.ArrowLeft
  }

  object ArrowRight extends ExternalComponentNoProps {
    val component = OcticonsJS.ArrowRight
  }



  object Database extends ExternalComponentNoProps {
    val component = OcticonsJS.Database
  }

  object Settings extends ExternalComponentNoProps {
    val component = OcticonsJS.Settings
  }

  object Archive extends ExternalComponentNoProps {
    val component = OcticonsJS.Archive
  }

  object Trashcan extends ExternalComponentNoProps {
    val component = OcticonsJS.Trashcan
  }

}
