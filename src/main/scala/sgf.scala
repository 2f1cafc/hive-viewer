// package hiveviewer.import_export.sgf

// import hiveviewer.MoveParser
// import hiveviewer.import_export.Importer
// import hiveviewer.model._

// // https://www.red-bean.com/sgf/sgf4.html
// // attempting to use the format exported by boardspace.net

// case class PropValueRaw(raw: String)

// sealed trait PropValue
// case class PropValueNumber(n: Int) extends PropValue
// case class PropValueReal(n: Double) extends PropValue
// case class PropValueText(s: String) extends PropValue
// case class PropValueComposed(a: PropValue, b: PropValue) extends PropValue

// case class Collection(games: Seq[GameTree])
// case class GameTree(root: Node, nodes: Seq[Node], gameTrees: Seq[GameTree]) {
//   // some shortcuts for the root node properties
//   def gameType = root.propNumber("GM")
//   def gameInfo = root.propText("GC")

//   def allNodes = root +: nodes

// }

// case class Node(properties: Map[String, PropValueRaw], isRoot: Boolean = false) {
//   import SGFParser.PropValExtractor
//   def typedProperty[T <: PropValue](extractor: PropValExtractor.Ex[T])(name: String) = properties.get(name).flatMap(extractor)
//   val propNumber = typedProperty(PropValExtractor.number) _
//   val propText = typedProperty(PropValExtractor.text) _
// }

// object SGFParser {

//   import fastparse._

//   /*  private[sgf] */
//   object atoms {
//     import NoWhitespace._

//     def digits[_ : P] = CharIn("0-9").rep(1).!

//     def number[_ : P] = P(CharIn("+\\-").? ~ digits).!

//     def escapedSlash[_ : P] = P("\\\\".!)
//     def escapedSquareBracket[_ : P] = P("\\]".!)
//     // when doing raw parsing, only \] and \\ are handled
//     def rawEscaped[_ : P] = P(escapedSlash | escapedSquareBracket)

//     def escapedAny[_:P] = P(("\\" ~ AnyChar).!)

//     def text[_ : P](escapeHandler: String => String) = P((escapedAny.map(escapeHandler) | (!CharIn("]") ~ AnyChar.!)).rep).map(_.mkString)

//     def formattedText[_ : P] = P(text {
//       case "\\\n" => ""
//       case any if any.startsWith("\\") => any.tail.take(1)
//       case any => any
//     })

//     def propIdent[_ : P] = P(CharIn("A-Z0-9").rep(1).!)

//     def propValueText[_ : P] = formattedText.map(PropValueText.apply)

//     def propValueNum[_ : P]: P[PropValueNumber] =
//       P((number.map { numStr => PropValueNumber(numStr.toInt) }))

//     def propValueReal[_ : P] = P((number ~ "." ~ digits) map {
//       case (left, right) => PropValueReal(s"$left.$right".toDouble)
//     })

//     def whiteSpace[_ : P] = P(CharIn(" \t\n"))

//     def propValueComposed[_ : P]: P[PropValue] = ???
//       //P((propValue1 ~ whiteSpace.rep ~ ":" ~ whiteSpace.rep ~ propValue1).map { case (a, b) => PropValueComposed(a, b) })

//     def propValueRaw[_ : P]: P[PropValueRaw] = P(("[" ~ (rawEscaped | (!"]" ~ AnyChar)).rep.! ~ "]").map(PropValueRaw.apply))

//     // not composed
//     // def propValue1[_ : P]: P[PropValue] = P(propValueReal | propValueNum | propValueText)
//     // def propValue[_ : P] = P("[" ~ (propValueComposed | propValue1) ~ "]")
//   }

//   /*  private[sgf] */
//   object composites {
//     import MultiLineWhitespace._
//     import atoms._

//     def property[_ : P] = P(propIdent ~ propValueRaw)

//     def node[_ : P] = P(";" ~ property.rep.map(props => Node(props.toMap)))

//     def rootNode[_ : P] = node.map(n => n.copy(isRoot = true))
//     def sequence[_ : P] = P(node.rep)

//     def gameTree[_ : P]: P[GameTree] = P(("(" ~ rootNode ~ sequence ~ gameTree.rep ~ ")").map {
//       case (rootNode, nodes, trees) => GameTree(rootNode, nodes, trees)
//     })
//   }

//   def apply(input: String): Either[String, GameTree] = {
//     parse(input.dropWhile(_.isWhitespace), composites.gameTree(_)) match {
//       case Parsed.Success(res, _) => Right(res)
//       case Parsed.Failure(msg, idx, extra) => Left(s"SGF: failed to parse game tree: $msg ($idx) [${extra.trace().longAggregateMsg}]")
//     }
//   }

//   def typedValue[T <: PropValue](parser: P[_] => P[T])(input: PropValueRaw): Option[T] = {
//     parse(input.raw, parser(_)) match {
//       case Parsed.Success(value, _) => Some(value)
//       case _: Parsed.Failure => None
//     }
//   }

//   object PropValExtractor {
//     type Ex[T <: PropValue] = PropValueRaw => Option[T]

//     val number: Ex[PropValueNumber] = typedValue(atoms.propValueNum(_)) _
//     val text: Ex[PropValueText] = typedValue(atoms.propValueText(_)) _
//   }

// }

// abstract class SGFImporter {

//   import hiveviewer.model

//   def extractMoveFromNode(node: Node): Option[model.Move]

//   def getComment(node: Node): Option[String] = node.propText("C").map(_.s)

//   def doImportFromSGF(sgfGameTree: GameTree) =
//     model.GameLog(sgfGameTree.nodes.map(extractMoveFromNode)
//       .flatten.toList.reverse)
// }

// object SGFImporter extends Importer {
//   val boardSpaceGameNumber = 27
//   def doImport(input: String) = SGFParser(input).map { sgfGameTree =>
//     if(sgfGameTree.gameType.exists(_.n == boardSpaceGameNumber))
//       SGFBoardSpaceImporter.doImportFromSGF(sgfGameTree)
//     else
//       SGFSimpleImporter.doImportFromSGF(sgfGameTree)
//   }
// }

// // this doesn't work yet because boardspace doesn't indicate the
// // target piece when a beetle climbs on top of it, just the
// // coordinates, and I haven't worked out how to use the co-ordinates
// // yet.
// object SGFBoardSpaceImporter extends SGFImporter {
//   // example: ;P0[5 dropb wS1 O 14 bS1/]

//   def extractMoveFromNode(node: Node) = {
//     ((node.propText("P0") orElse node.propText("P1")) flatMap { prop =>
//       val fields = "\\s+".r.split(prop.s)
//       fields match {
//         case a @ Array(_, "dropb", piece, _, _, ".") =>
//           // "." marks initial position, which in our notation is
//           // represented as just an empty string (i.e. no position)
//           MoveParser(s"$piece").toOption
//         case Array(_, "dropb", piece, _, _, loc) =>
//           MoveParser(s"$piece $loc").toOption
//         case _ => None
//       }
//     }).map(_.copy(comment = getComment(node)))
//   }
// }

// object SGFSimpleImporter extends SGFImporter {
//   def extractMoveFromNode(node: Node) = {
//     (node.propText("B") orElse node.propText("W")) flatMap { prop =>
//       MoveParser(prop.s).toOption.map(_.copy(comment = getComment(node)))
//     }
//   }
// }

// object SGFExporter {
//   def exportNode(node: Node): String = {
//     ";" + node.properties.map { case (k, v) =>
//       s"$k[${v.raw}]"
//     }.mkString("\n")
//   }

//   def exportGame(game: GameTree): String =
//     "(" + game.allNodes.map(exportNode).mkString("\n") + ")"

//   def exportCollection(col: Collection): String =
//     col.games.map(exportGame).mkString("\n")

//   def exportGameLog(log: GameLog) = {
//     val moves = log.moves.collect { case m: Move => m }
//     GameTree(
//       root = Node(Map.empty, true),
//       nodes = moves.reverse.zipWithIndex.map { case (m, idx) =>
//         val col = if(idx % 2 == 0) "W" else "B"
//         Node(Map(col -> PropValueRaw(m.toString))),
//       },
//       gameTrees = Nil
//     )
//   }
// }
