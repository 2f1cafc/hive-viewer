package hiveviewer.components

import hiveviewer.model._
import slinky.core._
import slinky.core.annotations._
import slinky.web.svg

case class Dimensions(width: Double, height: Double)

@react class Piece extends StatelessComponent {
  case class Props(id: PieceId, x: Double, y: Double, pieceDim: Dimensions)

  val pieceWidth = props.pieceDim.width
  val pieceHeight = props.pieceDim.height
  def centreTransformation(w: Double = pieceWidth, h: Double = pieceHeight) =
    svg.transform := s"translate(-${w / 2} -${h / 2})"

  def render = {
    import svg._
    g()(
      use(width := pieceWidth.toString, height := pieceHeight.toString,
        x := props.x,
        y := props.y,
        attrs.xlinkHref := s"img/bugs.svg#${props.id}",
        centreTransformation(pieceWidth, pieceHeight))
    )
  }
}

@react class Board extends StatelessComponent {
  type PieceMap = Seq[(PieceId, Double, Double)] // maps pieces to actual x,y coords on the display

  case class Props(boardDim: Dimensions, pieceLocations: BoardMap, zoomLevel: Double)

  val boardWidth = props.boardDim.width
  val boardHeight = props.boardDim.height

  // this might belong in the props, and then get passed down to the actual piece
  val pieceDim = Dimensions(width = 70.4, height = 80)
  val pieceWidth  = pieceDim.width
  val pieceHeight = pieceDim.height

  val hoff = pieceWidth * 0.5
  val voff = pieceHeight * 0.8

  val gap = pieceWidth * 0.05

  def render = {
    import svg.{ svg => SVG, _ }

    val piecesOnBoard = props.pieceLocations.collect {
      case (pieceId, h: HexLoc) => pieceId -> h
    }
    val pieces = placePieces(piecesOnBoard).map { case (pieceId, x, y) => Piece(pieceId, x, y, pieceDim).withKey(pieceId.toString) }

    // val stacks = piecesOnBoard.toSeq.map { case (pieceId, hex) =>
    //   pieceId -> hex.h
    // }.groupBy { case (pieceId, _) => val hex = piecesOnBoard(pieceId); (hex.x, hex.y, hex.z) }

    SVG(
      id := "board",
      preserveAspectRatio := "xMinYMin",
      viewBox := s"0 0 ${boardWidth} ${boardHeight}",
    ) (
      g(transform := s"translate(${boardWidth / 2} ${boardHeight / 2}) scale(${props.zoomLevel})")(pieces)
    )
    //     pieceMap.map {
    //     case (pieceId, (x, y)) => Piece(pieceId, x, y, 0, pieceDim).withKey(pieceId.toString) })
  }

  // def putPieceAt(pieceId: PieceId, x: Double, y: Double, cur: PieceMap) =
  //   cur.updated(pieceId, (x, y))

  val heightOffset = 7 // for stacks of pieces

  def putPieceAtHex(pieceId: PieceId, hexLoc: HexLoc) = {
    // val x = 0
    // val y = 0
    val x = (
      (hexLoc.x * (pieceWidth + gap) + hexLoc.y * (hoff + gap) + hexLoc.z * hoff)
        + heightOffset * hexLoc.h
    )
    val y = (
      (hexLoc.z * (voff + gap) - hexLoc.y * (voff + gap))
        + heightOffset * hexLoc.h
    )

    (pieceId, x, y)

  }

  // take the game map tree and convert it into a list of pieces and
  // thier x,y coords (in other, convert the logical tree into a shape
  // that can be drawn on the screen).
  def placePieces(pieceLocs: Map[PieceId, HexLoc]): PieceMap =
    pieceLocs.toSeq.sortBy({ case (_, hex) => hex.h })
      .map { case (pieceId, hex) => putPieceAtHex(pieceId, hex) }

//      .foldLeft(acc) { (acc, pieceLoc) => putPieceAtHex(pieceLoc._1, pieceLoc._2,  acc) }

  // final def placePieces(pieceLocs: GameMapNode, loc: Option[Location] = None, acc: PieceMap = Map.empty): PieceMap = {
  //   val withThisPiece: PieceMap = loc.map(putPieceBy(pieceLocs.pieceId, _, acc)).getOrElse(putPieceInitial(pieceTree.pieceId))
  //   pieceLocs.adjacents.foldLeft(withThisPiece) { (acc, direction) =>
  //     val (side, branch) = direction
  //     placePieces(branch, Some(Location(pieceTree.pieceId, side)), acc)
  //   }
// if(acc.isEmpty) /* first piece */ putPieceInitial(pieceTree.pieceId) else
//     println(withThisPiece)
//     withThisPiece
//  }

//  def putPieces(pieces: GameMap) = ??? // was thinking here that GameMap is the tree of pieces, each with 6 branches for N,E,SE etc

}
