package hiveviewer.components

import slinky.core.annotations.react
import slinky.core._
import slinky.web.html._

@react class CommentBox extends Component {
  case class Props(initialCommentText: String, updateCommentCB: (String) => Unit)
  case class State(commentText: String, editing: Boolean)

  def initialState = State(commentText = props.initialCommentText, editing = false)

  def toggleEditing() = setState(state.copy(editing = !state.editing))

  def setComment(newValue: String) = setState(state.copy(commentText = newValue))

  def render = {
    CollapsableCard(header = h6("Comment",
      span(className := "float-right")(
        Button(Button.size := "sm", Button.variant := "secondary", onClick := { e => e.stopPropagation(); toggleEditing() })("Edit"))))(
      Card.Body(
        (if(state.editing)
          Form(onSubmit := { e => e.preventDefault(); props.updateCommentCB(state.commentText); toggleEditing() })(
            Form.Control(Form.attr.as := "textarea", Form.attr.rows := 5, value := state.commentText, onChange := { e => setComment(e.target.value) }),
            Button(Button.variant := "primary", Button.`type` := "submit")("Submit")
          )
        else
          div(span(state.commentText))
        )
      )
    )
  }

}

object CommentBox {
  override val getDerivedStateFromProps =
    (nextProps: Props, prevState: State) => {
      if(prevState.editing) prevState
      else prevState.copy(commentText = nextProps.initialCommentText)
    }
}
