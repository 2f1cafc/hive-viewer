package hiveviewer.components

import hiveviewer.GameViewerState

import sabaki.gametreeFacade._
import sabaki.gametreeFacade.GameTree._
import sabaki.types._

import hiveviewer.Config
import hiveviewer.import_export.MoveExtractor
import slinky.core.annotations.react
import slinky.core._

import org.scalajs.dom

import scalajs.js
import js.JSConverters._
import sabaki.sgf.Sgf

import hiveviewer.model._
import hiveviewer.database.PouchGameDB
import scala.concurrent.Future
import slinky.web.html.div

object attrs {
  val xlinkHref = CustomAttribute[String]("xlinkHref")
}

@react class ControlPanelNav extends StatelessComponent {
  case class Props(selectNav: (String) => Unit, selectedNav: String)

  def render = {
    Nav(
      onSelect = ((key, _) => props.selectNav(key)),
      variant = "tabs",
      activeKey = Some(props.selectedNav)
    )(
      Nav.Item(Nav.Link(eventKey = "game-info")("Info")),
      Nav.Item(Nav.Link(eventKey = "current")("Current")),
      Nav.Item(Nav.Link(eventKey = "moves")("Moves")),
      Nav.Item(Nav.Link(eventKey = "game-list")("Game List")),
    )
  }
}

@react class GameViewer extends Component {
  case class Props(initialTree: GameTree[Int], moveExtractor: MoveExtractor, initialConfig: Config, db: PouchGameDB)
  case class State(tree: GameTree[Int], config: Config, configModalOpen: Boolean = false, selectedNav: String)

  implicit val ec = scala.concurrent.ExecutionContext.global

  def config = state.config

  def viewerState = state.config.viewerState

  def zoomLevel = viewerState.zoomLevel
  def current = viewerState.currentNodeId.getOrElse(state.tree.root.id)
  def currents = viewerState.currentPath
  def tree = state.tree

  def defaultTab = "game-info"

  // whenever we get a new state, copy it to the cookie

  override def componentDidUpdate(prevProps: Props, prevState: State) =
    Config.toCookie(state.config)

  def initialState = {
    val initialTree = props.initialTree
    val initialConfig = props.initialConfig.modifyViewerState { st =>
      // if for some reason the currentNodeId points to a non-existent node, remove it
      st.copy(currentNodeId = st.currentNodeId.filter(id => initialTree.get(id) != null))
    }
    State(tree = initialTree, config = initialConfig, selectedNav = defaultTab)
  }

  def modifyConfig(f: Config => Config) = setState((prevState) => {
    prevState.copy(config = f(prevState.config))
  })

  def modifyViewerState(f: GameViewerState => GameViewerState) =
    modifyConfig { prevCfg =>
      prevCfg.copy(viewerState = f(prevCfg.viewerState))
    }

  def selectVariation(childNode: NodeObject[Int]) = {
    val newCurrents = currents.updated(childNode.parentId.toString, childNode.id).toJSDictionary
    modifyViewerState(_.copy(currentPath = newCurrents))
  }

  def updateTree(newTree: GameTree[Int]) = {
    modifyViewerState(_.copy(rootNode = Some(newTree.root)))
    setState(_.copy(tree = newTree))
  }

  def mutateTree(f: Draft[Int] => Unit) = {
    val newTree = state.tree.mutate(f)
    updateTree(newTree)
  }

  def addNode(parentId: Int, data: NodeData) = {
    var newId: Int = tree.root.id
    mutateTree { draft =>
      newId = draft.appendNode(parentId, data)
    }
    modifyViewerState { prevState =>
      // make sure the new node is on the current line before we select it
      val newCurrents = prevState.currentPath.updated(parentId.toString, newId)
      prevState.copy(currentNodeId = Some(newId), currentPath = newCurrents.toJSDictionary)
    }
  }

  def navigateNodes(direction: Int) = {
    val res = tree.navigate(current, direction, currents)
    if(res != null) modifyViewerState(_.copy(currentNodeId = Some(res.id)))
  }

  def remoteDB = config.remoteDbName map { url => new pouchdb.PouchDB(url, config.remoteDbUser, config.remoteDbPass) }

  def syncDB() = remoteDB match {
    case Some(rdb) =>
      props.db.syncWith(rdb).foreach {
        case Left(err) => dom.window.alert(s"Failed to sync: $err")
        case Right(_) => dom.window.alert(s"Sync successful")
      }
    case None => dom.window.alert(s"configure remote db first")
  }

  def deleteNode(nodeId: Int) = {
    // if the node that we are deleting was the current one, remove it
    // from the currents
    val node = tree.get(nodeId)
    val parentId = node.parentId.toString
    val newCurrents = currents.get(parentId) match {
      case Some(`nodeId`) => (currents - parentId).toJSDictionary
      case _ => currents
    }
    mutateTree(_.removeNode(nodeId))
    val newCurrent = if(current == nodeId) node.parentId else  current
    modifyViewerState(_.copy(currentPath = newCurrents, currentNodeId = Some(newCurrent)))
  }

  def saveSgf = Sgf.stringify(js.Array(state.tree.root))

  def saveGameTree() = {
    state.tree.root.prop1[String]("GN") match {
      case Some(name) =>
        props.db.updateGame(name, state.tree, viewerState.docRevision).foreach {
          case Right(doc) =>
            val rev = doc.rev.asInstanceOf[String]
            println(s"saved game rev: $rev")
            modifyViewerState(_.copy(docRevision = Some(rev)))
          case Left(err) => println(s"unable to save game: $err")
        }
      case None => org.scalajs.dom.window.alert("no game name")
    }
  }

  def loadSgfData(data: String) = {
    // TODO : error handling if bad input
    val newTree = GameTree.fromSgf(data)
    updateTree(newTree)
    val newCurrent = newTree.lastNode(js.Dictionary.empty).id
    modifyViewerState(
      _.copy(currentNodeId = Some(newCurrent),
        currentPath = js.Dictionary.empty,
      currentGameName = newTree.root.prop1[String]("GN")))
  }

  def root = state.tree.root

  def moveList = tree.listCurrentNodes(currents)
    .toIterator
    .takeWhile(_.parentId != current) // stop *after* the current node
    .map(node => props.moveExtractor.extractText(node.id, tree))
    .toList
    .flatten

  def jumpToNode(id: Int) = modifyViewerState(_.copy(currentNodeId = Some(id)))

  val boardWidth  = 800
  val boardHeight = 700

  def boardMap: BoardMap = {
    val currentNodes = state.tree.listCurrentNodes(currents).toIterator.takeWhile(_.parentId != current)
    currentNodes.foldLeft(Map.empty: BoardMap) { (acc, node) =>
      props.moveExtractor.extractMove(acc, node.id, state.tree)
    }
  }

  def getComment(nodeId : Int): Option[String] = tree.getOpt(nodeId).flatMap(_.prop1[String]("C"))

  def setComment(nodeId: Int, newValue: String) =
    mutateTree { draft =>
      if(newValue.isEmpty)
        draft.removeProperty(nodeId, "C")
      else
        draft.updateProperty(nodeId, "C", js.Array(newValue))
    }

  def changeZoom(step: Double) =
    modifyViewerState(_.copy(zoomLevel = Math.max(0.1, zoomLevel + step)))

  def loadGames(): Future[List[String]] =
    props.db.listGames("").map { res =>
      res.left.map(_ => Nil).merge
    }

  def loadGameFromDb(name: String): Unit = {
    println(s"loading game: $name")
    props.db.loadGame(name).foreach {
      case Right((rev, tree)) =>
        println(s"Loaded revision $rev")
        reset(Some(tree))
        modifyViewerState(_.copy(docRevision = Some(rev)))
      case Left(err) => println(s"Failed to load: $err")
    }
  }

  def deleteGameFromDb(name: String): Unit = {
    viewerState.docRevision match {
      case Some(rev) =>
        if(dom.window.confirm(s"Delete game: $name?")) props.db.deleteGame(name, rev)
      case None =>
        dom.window.alert("Not loaded from db (no document rev stored)")
    }
  }

  def reset(tree: Option[GameTree[Int]] = None) = {
    val newTree = tree.getOrElse(new GameTree[Int]())
    updateTree(newTree)
    modifyViewerState(_ => GameViewerState(rootNode = Some(newTree.root)))
    setState(_.copy(selectedNav = defaultTab))
  }

  def render = {
    div(
      ConfigDialog(  // modal -> only shown if in use
        show = state.configModalOpen,
        config,
        closeCb = (() => setState(_.copy(configModalOpen = false))),
        modifyConfigCb = modifyConfig _),
      Row()(
        Col()(
          Board(Dimensions(width = boardWidth, height = boardHeight), boardMap, zoomLevel)
        ),
        Col(Col.md := 4)(
          Row()(
            Col()(CommandBar(
              saveGameTreeCb = () => saveGameTree(),
              saveSgfCb = saveSgf _,
              loadSgfDataCb = loadSgfData _,
              navigateCb = navigateNodes _,
              changeZoomCb = changeZoom _,
              showConfigModalCb = (() => setState(_.copy(configModalOpen = true))),
              resetCb = (() => reset()),
              syncDbCb = syncDB _))
          ),
          Row()(
            Col()(ControlPanelNav(selectedNav = state.selectedNav, selectNav = ((key) => setState(_.copy(selectedNav = key)))))
          ),
          Row()(
            Col()(
              state.selectedNav match {
                case "game-list" =>
                  GameList(loadGames _, loadGameFromDb _, deleteGameFromDb _)
                case "moves" =>
                  MoveList(tree, props.moveExtractor, currents, current, selectVariation, addNode, deleteNode, jumpToNode)
                case "game-info" =>
                  GameInfoPanel(tree, updateTree _)
                case _ =>
                  div(
                    Row()(
                      Col(CurrentNode(tree, props.moveExtractor, currents, current,
                        selectVariation, addNode, deleteNode, jumpToNode,
                        deleteNode))
                    ),
                    Row()(
                    Col(CommentBox(
                      initialCommentText = getComment(current).getOrElse(""),
                      updateCommentCB = (newText) => setComment(current, newText)))
                    )
                  )
              }
            )
          ),
          // Row()(
          //   Col()(GameList(loadGames _, loadGameFromDb _, deleteGameFromDb _))
          // ),
          // Row()(
          //   Col()()
          // ),
          // Row()(
          //   Col()(CommentBox(
          //     initialCommentText = getComment(current).getOrElse(""),
          //     updateCommentCB = (newText) => setComment(current, newText)))
          // ),
          // Row()(
          //   Col()(MoveList(tree, props.moveExtractor, currents, current,
          //     selectVariation, addNode, deleteNode, jumpToNode))
          // ),
        )
      )
    )
  }
}
