package hiveviewer.components

import slinky.core.annotations.react
import slinky.core._
import scala.concurrent.{ Future, Promise }
import org.scalajs.dom
import slinky.core.facade.React
import slinky.web.html._

@react class SGFMenu extends Component {

  case class Props(
    saveSgfCb: () => String,
    loadSgfDataCb: (String) => Unit,
  )

  implicit val ec = scala.concurrent.ExecutionContext.global

  case class State(exportLink: Option[String], fileName: Option[String])

  def initialState = State(exportLink = None, fileName = None)

  def readFileContents(f: dom.File): Future[String] = {
    val p = Promise[String]()
    val rdr = new dom.FileReader()
    rdr.onload = (_) => {
      p.success(rdr.result.asInstanceOf[String])
    }
    rdr.readAsText(f)
    p.future
  }

  def fileChangeHandler(e: SyntheticEvent[dom.raw.HTMLInputElement, dom.Event]) = {
    if(e.target.files.length > 0) {
      // we only support one file at a time
      readFileContents(e.target.files(0)).foreach(props.loadSgfDataCb)
      setState(state.copy(fileName = Some(e.target.files(0).name)))
    }
  }

  def fileImport = {
    val inputRef = React.createRef[dom.html.Input]
    val fileInput =
      input(ref := inputRef, `type` := "file",
        onChange := (fileChangeHandler(_)),
        className := "d-none")
    (inputRef, fileInput)
  }

  def saveSgf = {
    if(state.exportLink.isDefined) {
      setState(state.copy(exportLink = None))
    } else {
      val sgfSrc = props.saveSgfCb()
      setState(state.copy(exportLink =
        Some(s"data:text/plain;base64,${dom.window.btoa(sgfSrc)}")))
    }
  }

  def exportLinkAnchor = state.exportLink.map { url =>
    val downloadWithFname = CustomAttribute[String]("download") // seems slinky only has the boolean version of this attr
    Button(
      Button.variant := "secondary",
      onClick := (() => {
        setState(state.copy(exportLink = None))
      }),
      Button.href := url,
      downloadWithFname := state.fileName.getOrElse("hivegame.sgf"))("ready")
  }

  def render = {
    val (inputRef, fileInput) = fileImport

    ButtonGroup(className := "mr-2")(
      fileInput,
      Dropdown()(
        Dropdown.Toggle(Octicon(Octicon.Archive())),
        Dropdown.Menu()(
          Dropdown.Item(onClick := (() => inputRef.current.click()))("Import SGF file"),
          Dropdown.Item(onClick := (saveSgf _))("Export SGF file"),
        )
      ),
      exportLinkAnchor,
    )
  }
}

@react class DbMenu extends StatelessComponent {
  case class Props(saveGameTreeCb: () => Unit, syncDbCb: () => Unit)

  def render = {
    Dropdown()(
      Dropdown.Toggle(Octicon(Octicon.Database())),
      Dropdown.Menu()(
        Dropdown.Item(onClick := props.saveGameTreeCb)("Save to DB"),
        Dropdown.Item(onClick := props.syncDbCb)("Sync")
      )
    )
  }
}

@react class CommandBar extends StatelessComponent {
  case class Props(
    saveGameTreeCb: () => Unit,
    saveSgfCb: () => String,
    loadSgfDataCb: (String) => Unit,
    navigateCb: (Int) => Unit,
    changeZoomCb: (Double) => Unit,
    showConfigModalCb: () => Unit,
    resetCb: () => Unit,
    syncDbCb: () => Unit)

  def render = {
    div(
      ButtonToolbar(
        ButtonGroup(className := "mr-2")(
//          DbMenu(props.saveGameTreeCb, props.syncDbCb),
          SGFMenu(props.saveSgfCb, props.loadSgfDataCb),
          Button(onClick := props.resetCb)(Octicon(Octicon.Trashcan())),
        ),
        ButtonGroup(className := "mr-2")(
          Button(onClick := (() => props.navigateCb(-1)))(Octicon(Octicon.ArrowLeft())),
          // Button()("↑"),
          // Button()("↓"),
          Button(onClick := (() => props.navigateCb(1)))(Octicon(Octicon.ArrowRight())),
        ),
        ButtonGroup(className := "mr-2")(
          Button(onClick := (() => props.changeZoomCb(-0.1)))("z"),
          Button(onClick := (() => props.changeZoomCb(0.1)))("Z")
        ),
        ButtonGroup(className := "mr-2")(
          Button(onClick := (() => props.showConfigModalCb()))(Octicon(Octicon.Settings()))
        )
      )
    )
  }

}
