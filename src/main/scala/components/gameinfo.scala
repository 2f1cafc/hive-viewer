package hiveviewer.components

import scala.scalajs.js
import slinky.core.annotations.react
import slinky.core._
import sabaki.gametreeFacade._
import slinky.web.html._
import slinky.core.facade.{ Hooks, Fragment }

@react class GameInfoForm extends Component {
  case class Props(currentTree: GameTree[Int], updateInfo: (GameTree[Int]) => Unit)
  case class State(tree: GameTree[Int])

  def initialState = State(tree = props.currentTree)

  def tree = state.tree
  def root = tree.root

  def getProp(name: String) = root.prop1[String](name)

  def setProp(name: String, value: String) = {
    setState { prevState =>
      val newTree = prevState.tree.mutate { draft =>
        if(value.isEmpty())
          draft.removeProperty(prevState.tree.root.id, name)
        else
          draft.updateProperty(prevState.tree.root.id, name, js.Array(value))
      }
      prevState.copy(tree = newTree)
    }
  }

  @react object InfoField {
    case class Props(label: String, property: String)

    def updateField(property: String, value: String) = setProp(property, value)

    val component = FunctionalComponent[Props] { fieldProps =>

      val (fieldValue, setFieldValue) = Hooks.useState(getProp(fieldProps.property).getOrElse(""))

      Fragment(
        Form.Label(fieldProps.label),
        Form.Control(
          value := fieldValue,
          onChange := { (ev) =>
            val value = ev.target.value
            setFieldValue(value)
            setProp(fieldProps.property, value)
          }
        )
      )
    }
  }

  def updateInfo() = props.updateInfo(tree)

  def render = {
    Form(onSubmit := { e => e.preventDefault(); updateInfo() })(
      InfoField(label = "Game Name", property = "GN"),
      InfoField(label = "White", property = "PW"),
      InfoField(label = "Black", property = "PB"),
      InfoField(label = "Date", property = "DT"),
      Button(Button.variant := "primary", Button.`type` := "submit")("Submit")
    )
  }
}

@react class GameInfoDisplay extends StatelessComponent {
  case class Props(tree: GameTree[Int])

  def root = props.tree.root

  def gameName = root.prop1[String]("GN")
  def playerName(colour: String) = root.prop1[String](s"P$colour")
  def dateString = root.prop1[String]("DT")

  def render =
    div(
      Card.Title(gameName),
      p(playerName("W").getOrElse("?") + " v " + playerName("B").getOrElse("?")),
      p(small(dateString))
    )
}

@react class GameInfoPanel extends Component {
  case class Props(tree: GameTree[Int], updateInfo: (GameTree[Int]) => Unit)
  case class State(editing: Boolean)

  def initialState = State(editing = false)

  // shortcut
  def root = props.tree.root

  def toggleEditing() = setState(state.copy(editing = !state.editing))

  def updateInfo(newTree: GameTree[Int]): Unit = {
    toggleEditing()
    props.updateInfo(newTree)
  }

  def render = {
    CollapsableCard(initialOpen = true,
      header = h6("Game Info", span(className := "float-right")(
        Button(Button.size := "sm", Button.variant := "secondary", onClick := { e => e.stopPropagation(); toggleEditing() })("Edit"))
      ))(
      Card.Body(
        (if(state.editing)
          GameInfoForm(props.tree, updateInfo _)
        else
          GameInfoDisplay(props.tree))
      )
    )
  }
}
