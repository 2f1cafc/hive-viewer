package hiveviewer.components

import slinky.core._
import slinky.core.annotations.react
import slinky.web.html._
import slinky.core.facade.ReactElement

import scala.scalajs.js

@react class CollapsableCard extends Component {

  case class Props(
    header: ReactElement,
    initialOpen: Boolean = true,
    style: js.Object = js.Object(),
    children: ReactElement)

  case class State(open: Boolean)

  def initialState = State(open = props.initialOpen)

  def render =
      Card()(
        Card.Header(onClick := (() => setState(State(open = !state.open))))(props.header),
        Collapse(in = state.open)(style := props.style, props.children)
      )
}
