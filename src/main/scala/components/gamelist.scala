package hiveviewer.components

// loading a game from the db

import slinky.core.facade.React
import slinky.core._
import slinky.core.annotations.react
import scala.concurrent.Future
import slinky.web.html.onClick
import slinky.web.html.className

trait GameViewerOps {
  def loadGameFromDb(name: String): Unit
  def listGames(): List[String]
//  def saveGameTree(): Unit
}

object GameViewerOps {
  val context = React.createContext[Option[GameViewerOps]](None)
}

@react class GameListEntryButtons extends StatelessComponent {
  case class Props(gameName: String, deleteCb: String => Unit)

  def render = {
    ButtonToolbar(className := "float-right")(
      ButtonGroup()(Button.size := "sm", className := "mr-2")(
        Button(onClick := { e => e.stopPropagation(); props.deleteCb(props.gameName) },
          Button.variant := "secondary")("⌫")
      )
    )
  }
}

@react class GameListEntry extends StatelessComponent {
  case class Props(gameName: String, selectCb: String => Unit, deleteCb: String => Unit)
  def render =
    ListGroup.Item(onClick := (() => props.selectCb(props.gameName)))(props.gameName, GameListEntryButtons(props.gameName, props.deleteCb))

}

@react class GameList extends Component {
  implicit val ec = scala.concurrent.ExecutionContext.global

  case class Props(
    listGamesCb: () => Future[List[String]],
    selectGameCb: String => Unit,
    deleteGameCb: String => Unit
  )

  case class State(gamesList: List[String])
  def initialState = State(Nil)

  def setGamesList(newGamesList: List[String]) =
    setState(state.copy(gamesList = newGamesList))

  override def componentDidMount() = {
    println("gameList mounted")
    props.listGamesCb().foreach(setGamesList)
  }

  def render = {
    CollapsableCard(initialOpen = true, header = "Game List")(
      ListGroup(
        state.gamesList.zipWithIndex.map {
          case (gameName, idx) => GameListEntry(gameName, props.selectGameCb, props.deleteGameCb)
              .withKey(s"game-list-$idx")
        }
      )
    )
  }
}
