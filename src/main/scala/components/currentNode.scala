package hiveviewer.components

import slinky.core._
import sabaki.gametreeFacade._
import slinky.core.annotations.react
import sabaki.types._
import slinky.web.html._
import scala.scalajs.js

import hiveviewer.import_export.MoveExtractor

@react class CurrentNode extends StatelessComponent {
  case class Props(tree: GameTree[Int],
    moveExtractor: MoveExtractor,
    currents: js.Dictionary[Int],
    currentNode: Int,
    selectVariationCB: (NodeObject[Int]) => Unit,
    addNodeCB: (Int, NodeData) => Unit,
    removeNodeCB: (Int) => Unit,
    jumpToNodeCB: (Int) => Unit,
    deleteNodeCB: (Int) => Unit)

  def node = props.tree.get(props.currentNode)
  def moveNum = props.tree.getLevel(props.currentNode)

  def moveText = props.moveExtractor.extractText(props.currentNode, props.tree)

  def render = {
    Card()(
      MoveListEntry(props.tree, props.currents, moveText.getOrElse(""),
        moveNum, node, props.selectVariationCB, props.jumpToNodeCB,
        props.addNodeCB, props.deleteNodeCB, props.moveExtractor,
        false
      )
    )
  }
}
