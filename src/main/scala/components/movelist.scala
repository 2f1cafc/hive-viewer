package hiveviewer.components

import slinky.core._
import slinky.core.annotations._
import sabaki.gametreeFacade._
import sabaki.types._
import scalajs.js
import hiveviewer.import_export.MoveExtractor
import slinky.web.html._

@react class MoveListEntry extends Component {
  case class Props(
    tree: GameTree[Int], currents: js.Dictionary[Int], moveText: String, moveNum: Int,
    node: NodeObject[Int], selectVariationCB: (NodeObject[Int]) => Unit, jumpToNodeCB: (Int) => Unit,
    addNodeCB: (Int, NodeData) => Unit, deleteNodeCB: (Int) => Unit, moveExtractor: MoveExtractor,
    isCurrent: Boolean = false)

  case class State(adding: Boolean)

  def initialState = State(adding = false)

  def node = props.node

  def childNodeButtons = {
    /* if there is only one child, we don't need to bother showing the
     * buttons (as they are only useful for selecting from more than
     * one child) */
    val childButtons =
      if(node.children.length > 1) node.children.toList.zipWithIndex.map {
        case (childNode, idx) =>
          Button(
            key := s"move-list-button-${node.id}-${childNode.id}",
            Button.variant := "outline-secondary",
            onClick := { (e) => e.stopPropagation(); props.selectVariationCB(childNode) },
            className :=
              (if(props.tree.onCurrentLine(childNode.id, props.currents)) Some("active") else None)
          )((idx + 1).toString)
      } else Nil

    ButtonGroup(Button.size := "sm", className := "mr-2")(childButtons)
  }

  def isRoot = node.id == props.tree.root.id

  def deleteNodeButton =
    if(!isRoot)
      Some(Button(onClick := { (e) => e.stopPropagation(); props.deleteNodeCB(node.id) },
        Button.variant := "dark")("⌫"))
    else
      None

  def addNodeButton =
    Button(
      onClick := { (e) => e.stopPropagation(); toggleEditing },
      Button.variant := "dark" // (if(state.adding) "primary" else "primary"
    )("+")

  def modifyNodeButtons = ButtonGroup(Button.size := "sm")(deleteNodeButton, addNodeButton)

  def toggleEditing() = setState(state.copy(adding = !state.adding))

  def addNodeSubmit(parentId: Int, data: NodeData) = {
    props.addNodeCB(parentId, data)
    toggleEditing()
  }

  def render = {
    val nodeKey = "move-list-" + node.id
    val nodeToAddTo =
      if(isRoot) props.tree.lastNode(props.currents).id
      else node.id
    ListGroup.Item(
      key := nodeKey,
      ListGroup.active := props.isCurrent,
      onClick := (() => props.jumpToNodeCB(node.id)))(
      (if(!isRoot) span(s"${props.moveNum}. ${props.moveText}") else em("root")),
      ButtonToolbar(className := "float-right")(childNodeButtons, modifyNodeButtons),
        (if(state.adding) Some(AddNodeForm(
          nodeToAddTo,
          (if(props.tree.getLevel(nodeToAddTo) % 2 == 0) "W" else "B"),
          addNodeSubmit _, props.moveExtractor)) else None))
  }
}

@react class MoveList extends StatelessComponent {
  case class Props(tree: GameTree[Int], moveExtractor: MoveExtractor, currents: js.Dictionary[Int],
    currentNode: Int, selectVariationCB: (NodeObject[Int]) => Unit, addNodeCB: (Int, NodeData) => Unit,
    removeNodeCB: (Int) => Unit, jumpToNodeCB: (Int) => Unit)

  def render = {
    val rootItem = MoveListEntry(props.tree, props.currents, "", 0, props.tree.root, props.selectVariationCB,
            props.jumpToNodeCB, props.addNodeCB, props.removeNodeCB, props.moveExtractor,
            isCurrent = false).withKey(s"move-list-entry-${props.tree.root.id}")

    val moveNum = Iterator.from(1)
    val moves = props.tree.listCurrentNodes(props.currents)
      .toIterator.map { node =>
        props.moveExtractor.extractText(node.id, props.tree).map { move =>
          MoveListEntry(
            props.tree, props.currents, move, moveNum.next, node, props.selectVariationCB,
            props.jumpToNodeCB, props.addNodeCB, props.removeNodeCB, props.moveExtractor,
            isCurrent = (props.currentNode == node.id)
          ).withKey(s"move-list-entry-${node.id}")
        }
      }.flatten.toList
    CollapsableCard(
      style = js.Dynamic.literal(maxHeight = "600px", overflow = "scroll"),
      header = h6("Move list"),
      initialOpen = true)(
      ListGroup(rootItem :: moves)
    )
  }
}

@react class AddNodeForm extends Component {

  import org.scalajs.dom

  case class State(moveText: String, isValid: Boolean)
  case class Props(nodeId: Int, player: String, addNodeCB: (Int, NodeData) => Unit, moveExtractor: MoveExtractor)

  def initialState = State(moveText = "", isValid = false)

  def submitHandler(e: SyntheticEvent[dom.html.Form, dom.Event]) = {
    e.preventDefault()
    props.addNodeCB(props.nodeId, NodeData(props.player -> state.moveText))
  }

  def updateValue(value: String) = setState(state.copy(moveText = value, isValid = props.moveExtractor.isValidMove(value)))

  def render = {
    Form(onSubmit := (submitHandler(_)))(
      Form.Group(
        Form.Label("Add move"),
        Form.Control(`type` := "text",
          onChange := ((ev) => updateValue(ev.target.value))),
        Button(disabled = !state.isValid)(Button.variant := "success", `type` := "submit")("Add")
      )
    )
  }
}
