package hiveviewer.components

import slinky.core.annotations.react
import slinky.core._
import hiveviewer.Config
import slinky.web.html._

@react class ConfigField extends StatelessComponent {
  case class Props(
    label: String,
    get: () => String,
    set: String => Unit,
    `type`: Option[String] = None)

  def get() = props.get()
  def set(newValue: String) = props.set(newValue)

  def render = {
    div(
      Form.Label(props.label),
      Form.Control(value := get(),
        `type` := props.`type`,
        onChange := { (e) => set(e.target.value) }
      )
    )
  }
}

@react class ConfigDialog extends Component {
  case class Props(show: Boolean,
    currentConfig: Config,
    closeCb: () => Unit,
    modifyConfigCb: (Config => Config) => Unit)

  case class State(config: Config)
  def initialState = State(config = props.currentConfig)

  def submit() = {
    props.modifyConfigCb(_ => state.config)
    props.closeCb()
  }

  def cancel() = {
    setState(_.copy(config = props.currentConfig))
    props.closeCb()
  }

  def config = state.config
  def set(f: (Config, Option[String]) => Config) = { (newValue: String) =>
    setState { prevState =>
      prevState.copy(config = f(config, (if(newValue.isEmpty) None else Some(newValue))))
    }
  }

  def get(f: Config => Option[String]) = () => f(config).getOrElse("")

  def render = {
    Modal(show = props.show, onHide = cancel)(
      Modal.Header(closeButton = true)(Modal.Title("Configuration")),
      Modal.Body(
        Form(onSubmit := { e => e.preventDefault(); submit() })(
          ConfigField(
            label = "RemoteDB Url",
            get = get(_.remoteDbName),
            set = set((c, v) => c.copy(remoteDbName = v))
          ),
          ConfigField(
            label = "RemoteDB User",
            get = get(_.remoteDbUser),
            set = set((c, v) => c.copy(remoteDbUser = v))
          ),
          ConfigField(
            `type` = Some("password"),
            label = "RemoteDB Password",
            get = get(_.remoteDbPass),
            set = set((c, v) => c.copy(remoteDbPass = v))
          )
        )
      ),
      Modal.Footer(Button(Button.variant := "primary", onClick := submit _)("OK"))
    )
  }
}

// object ConfigDialog {
//   val setConfig = React.createContext[Config => Unit](identity)
// }
