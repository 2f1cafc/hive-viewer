package hiveviewer

import model._
import org.scalatest.{ FunSpec, EitherValues }

class ParseTest extends FunSpec with EitherValues {

  describe("moveParser") {
    it("should parse a move that goes on top of another piece") {
      assert(MoveParser("wB1 bA2").right.value ==
        Move(PieceId(Colours.White, Insects.Beetle, 1),
          Some(Location(PieceId(Colours.Black, Insects.Ant, 2), Sides.Atop))))
    }
  }

}
