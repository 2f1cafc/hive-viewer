package hiveviewer.import_export.sgf

import SGFParser._
import SGFParser.atoms._
import SGFParser.composites._

// minimal testing for the SGF parser
import org.scalatest.{ FunSpec, OptionValues }

import fastparse.{ Parsed, parse }

class SGFTest extends FunSpec with OptionValues {

  // asserts that a parse result is a success, and returns it if so
  // for further processing.
  implicit class ResultMatcher[T](res: Parsed[T]) {
    def succeeds: Parsed.Success[T] = res match {
      case s: Parsed.Success[_] => s
      case Parsed.Failure(msg, idx, extra) => fail(s"parser expected success, failed with: $msg : $idx « ${extra.traced.longAggregateMsg} »")
    }
    def fails: Parsed.Failure = res match {
      case f: Parsed.Failure => f
      case _: Parsed.Success[_] => fail("parser was expected to fail, but it succeeded")
    }
  }

  describe("propValue parsers") {

    describe("propValueRaw") {
      it("should parse entire contents") {
        assert(parse("[foo\n      \\bar]", propValueRaw(_)).succeeds.value == PropValueRaw("foo\n      \\bar"))
      }
      it("should allow escaping of ']'") {
        assert(parse("[foo\n   \\]   \\bar]", propValueRaw(_)).succeeds.value == PropValueRaw("foo\n   \\]   \\bar"))
      }
      it("should allow escaping of '\\'") {
        assert(parse("""[foo\bar\\]""", propValueRaw(_)).succeeds.value == PropValueRaw("""foo\bar\\"""))
        assert(parse("""[wA3 wG2\\]""", propValueRaw(_)).succeeds.value == PropValueRaw("""wA3 wG2\\"""))
      }
    }

    describe("number parser") {
      it("should parse a number with no sign") {
        assert(parse("109", propValueNum(_)).succeeds.value == PropValueNumber(109))
      }
      it("should parse a number with an explicit sign") {
        assert(parse("-109", propValueNum(_)).succeeds.value ==
          PropValueNumber(-109))
        assert(parse("+109", propValueNum(_)).succeeds.value ==
          PropValueNumber(109))
      }
    }

    describe("real number parser") {
      it("should parse a real number") {
        assert(parse("-109.74", propValueReal(_)).succeeds.value == PropValueReal(-109.74))
      }
    }

    describe("text parser") {
      it("should correctly parse formatted text") {
        assert(parse("won\nder\\\nful", propValueText(_)).succeeds.value == PropValueText("won\nderful"))
        assert(parse("""wA3 wG2\\""", propValueText(_)).succeeds.value == PropValueText("""wA3 wG2\"""))
      }

      it("should parse formatted text long example") {
        // example taken from the docs (but modified to not word wrap)
        val input = """Meijin NR: yeah, k4 is won\
derful
sweat NR: thank you! :\)
dada NR: yup. I like this move too. It's a move only to be expected from a pro. I really like it :)
jansteen 4d: Can anyone\
 explain [me\] k4?"""

        val expectedOutput = """Meijin NR: yeah, k4 is wonderful
sweat NR: thank you! :)
dada NR: yup. I like this move too. It's a move only to be expected from a pro. I really like it :)
jansteen 4d: Can anyone explain [me] k4?"""

        assert(parse(input, propValueText(_)).succeeds.value == PropValueText(expectedOutput))
      }
    }
  }

  describe("gametree parser") {
      val inputSrc = """(;P0[ time null]
P1[ time null]
GM[27]
CM[0,1]
SU[Hive]
GC[03]
GN[]
P0[ id p0]
P1[ id p1]
FF[4]
;P1[0 Start P1]
;P1[1 pick B 2 bG1]
;P1[2 dropb bG1 N 13 .]
;P1[3 done]
;P0[4 pick W 3 wB1]
;P0[5 dropb wB1 O 13 bG1-]
;P0[6 done]
;P1[7 pick B 0 bQ]
;P1[8 dropb bQ N 14 \\bG1]
;P1[9 done]
;P0[10 pick W 1 wA1]
;P0[11 dropb wA1 P 14 wB1/]
;P0[12 done]
;P1[13 pick B 1 bA1]
;P1[14 dropb bA1 M 12 /bG1]
;P1[15 done]
;P0[16 pick W 0 wQ]
;P0[17 dropb wQ O 12 wB1\\]
;P0[18 done]
;P1[19 pickb M 12 bA1]
;P1[20 dropb bA1 Q 15 wA1/]
;P1[21 done]
)
"""

    it("should successfully parse a complete sgf file") {
      val tree = parse(inputSrc, gameTree(_)).succeeds.value

      // some basic sanity checks on the parsed game tree
      assert(tree.gameType.value == PropValueNumber(27))
      assert(tree.gameInfo.value == PropValueText("03"))

    }

  }

  describe("node properties") {
    val node = Node(
      Map(
        "GM" -> "12",
        "GC" -> "sample game"
      ).view.mapValues(PropValueRaw.apply).toMap
    )

    describe("propNumber") {
      it("should handle a number property") {
        assert(node.propNumber("GM").value == PropValueNumber(12))
      }
      it("should be None on value that isn't a number") {
        assert(node.propNumber("GC").isEmpty)
      }
    }
  }
}
