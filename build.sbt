scalaVersion in ThisBuild := "2.13.1"

lazy val root = (project in file("."))
  .enablePlugins(ScalaJSPlugin, ScalaJSBundlerPlugin)
  .settings(
    name := "hive-viewer",
    // This is an application with a main method
    scalaJSUseMainModuleInitializer := true,

    scalacOptions += "-Wunused",

    libraryDependencies ++= Seq(
      "me.shadaj" %%% "slinky-core" % "0.6.5", // core React functionality, no React DOM
      "me.shadaj" %%% "slinky-web" % "0.6.5", // React DOM, HTML and SVG tags
      "org.scala-js" %%% "scalajs-dom" % "1.0.0",
      "com.lihaoyi" %%% "fastparse" % "2.2.4",
      "org.scalatest" %%% "scalatest" % "3.1.1" % "test"
    ),

    // required for the @react macro
    scalacOptions += "-Ymacro-annotations",

    webpackConfigFile := Some(baseDirectory.value / "webpack.custom.js"),

    npmDependencies in Compile ++= Seq(
      "react" -> "16.12.0",
      "react-dom" -> "16.12.0",
      "react-bootstrap" -> "1.0.1",
      "react-inline-editing" -> "1.0.10",
      "@primer/octicons-react" -> "9.6.0",
      "js-cookie" -> "2.2.1")
  )
  .dependsOn(sabaki, pouchdb)

lazy val sabaki = (project in file("./sabakihq"))
  .enablePlugins(ScalaJSPlugin, ScalaJSBundlerPlugin)
  .settings(
    libraryDependencies += "com.lihaoyi" %%% "utest" % "0.7.4" % "test",
    testFrameworks += new TestFramework("utest.runner.Framework"),
    npmDependencies in Compile ++= Seq(
      "@sabaki/immutable-gametree" -> "1.9.2",
      "@sabaki/sgf" -> "3.4.7"
    )
  )

lazy val pouchdb = (project in file("./pouchdbfacade"))
  .enablePlugins(ScalaJSPlugin, ScalaJSBundlerPlugin)
  .settings(
    organization := "com.pouchdb.scalajs",
    name := "pouchdb",
    npmDependencies in Compile ++= Seq(
      "pouchdb" -> "7.2.1",
    ),
    scalaJSUseMainModuleInitializer in Test := true,
    mainClass in Test := Some("pouchdb.Main"),
    scalaJSUseTestModuleInitializer in Test := false
  )
