/* grabs a game from a BordGameArena page */

/* build this with https://www.npmjs.com/package/bookmarklet (for example) */

let turnEls = document.querySelectorAll(".gamelogreview")
let moves = []
var count = 0
turnEls.forEach((e) => {
    let col = (count++) % 2 == 0 ? "W" : "B"
    let res = e.textContent.match(/\[(.+)\]/)
    if(res != null) {
        let move = res[1].replace("\\", "\\\\").replace(/\s*$/,"")
        moves.push(`;${col}[${move}]`)
    }
})
alert(`(;FF[4] GM[150] ${moves.join("\n")})`)
