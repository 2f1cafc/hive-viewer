package pouchdb

import scala.scalajs.js
import scalajs.js.annotation.JSImport
import scala.concurrent.Future
import scala.concurrent.ExecutionContext
import scala.scalajs.js.JSConverters._

trait DbInfo extends js.Object {
  val doc_count: js.UndefOr[Int] = js.undefined
  val db_name: js.UndefOr[String] = js.undefined
}

trait FindOptions extends js.Object {
  val selector: js.UndefOr[js.Object] = js.undefined
  val fields: js.UndefOr[Array[String]] = js.undefined
  val sort: js.UndefOr[Array[String]] = js.undefined
}

trait DocResult extends js.Object {
  val doc: js.Dynamic
  val id: String
  val key: String
}

trait AllDocsResult extends js.Object {
  val offset: Int
  val total_rows: Int
  val rows: js.Array[DocResult]
}

@js.native
trait EventEmitter extends js.Object {
  def on[T](eventName: String, cb: T => Unit): Unit = js.native
}

@JSImport("pouchdb", JSImport.Default)
@js.native
class PouchDBJS(
  db: String,
  options: js.UndefOr[js.Dynamic] = js.undefined
) extends js.Object {
  def info(): js.Promise[DbInfo] = js.native
  def put(doc: js.Dynamic): js.Promise[js.Dynamic] = js.native
  def get(id: js.Any): js.Promise[js.Dynamic] = js.native
  def find(opts: FindOptions): js.Promise[js.Dynamic] = js.native
  def allDocs(opts: AllDocsOptions): js.Promise[AllDocsResult] = js.native
  def close(): js.Promise[Unit] = js.native
  def remove(docId: String, docRev: String/* , [options] */):
      js.Promise[js.Dynamic] = js.native
  def sync(remoteDb: PouchDBJS): js.Promise[EventEmitter] = js.native
}

@js.native
trait PouchError extends js.Object {
  val status: Int = js.native
  val name: String = js.native
  val message: String = js.native
  val reason: String = js.native
}

trait AllDocsOptions extends js.Object

object AllDocsOptions {
  def empty = new AllDocsOptions {}
}

// this is a more scala-like interface to the above, using futures etc
class PouchDB(
  dbName: String,
  authName: Option[String] = None,
  authPassword: Option[String] = None
) {

  def this(dbName: String, authName: String, authPassword: String) = this(dbName, Some(authName), Some(authPassword))

  private val db = {
    val dbOptions = (for(name <- authName; pass <- authPassword) yield {
      js.Dynamic.literal(auth =
        js.Dynamic.literal(username = name, password = pass))
    }).orUndefined
    new PouchDBJS(dbName, dbOptions)
  }

  import PouchDB._

  def info(): Future[DbInfo] = db.info.toFuture
  def put(doc: js.Dynamic)(implicit ec: ExecutionContext): PouchResultF[js.Dynamic] = pouchResult(db.put(doc))
  def get(id: String)(implicit ec: ExecutionContext): PouchResultF[js.Dynamic] = pouchResult(db.get(id))
  def close()(implicit ec: ExecutionContext) = pouchResult(db.close())
  def allDocs(options: AllDocsOptions = AllDocsOptions.empty)(implicit ec: ExecutionContext):
      PouchResultF[AllDocsResult] = {
    pouchResult(db.allDocs(options))
  }
  def remove(id: String, rev: String)(implicit ec: ExecutionContext) =
    pouchResult(db.remove(id, rev))
  def find(opts: FindOptions) = db.find(opts).toFuture

  def sync(remoteDb: PouchDB)(implicit ec: ExecutionContext) = pouchResult(db.sync(remoteDb.db))
}

object PouchDB {
  type PouchResult[T] = Either[PouchError, T]
  type PouchResultF[T] = Future[PouchResult[T]]

  def pouchResult[T](f: js.Promise[T])(implicit ed: ExecutionContext): PouchResultF[T] = {
    f.toFuture.map(Right.apply).recover {
      case e: js.JavaScriptException => Left(e.exception.asInstanceOf[PouchError])
    }
  }

}
