package pouchdb

import scala.scalajs.js

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.Duration
import scala.scalajs.js.JSON
import scala.util.Success
import scala.util.Failure
import scala.scalajs.js.JavaScriptException

object Main {

  implicit val ec: ExecutionContext = ExecutionContext.global

  def main(args: Array[String]): Unit = {
    val db = new PouchDB("foo")

    val infoF = db.info()
    val getF = db.get("foo")

    val res = for {
      info <- infoF
      docEither <- getF
      _ <- db.close()
    } yield {
      println(s"docs: ${info.doc_count}")
      docEither match {
        case Right(doc) => println(s"got doc: $doc")
        case Left(err) => println(s"Error (${err.status}): ${err.message}")
      }
    }

//    Await.ready(res, Duration.Inf)

  }
}
