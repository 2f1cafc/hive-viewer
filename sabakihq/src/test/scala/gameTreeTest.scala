package sabaki.gametree

import utest._
import sabaki.sgf.Sgf
import scalajs.js

object GameTreeTests extends TestSuite {

  val tests = Tests {
    test("game tree") {
      test("different object after mutate which mutates") {
        val tree = GameTreeModel.newTree()
        val newTree = tree.mutate { draft =>
          draft.appendNode(0, "C" -> List("foo"))
        }
        assert(tree ne newTree)
      }
      test("same object if callback doesn't mutate") {
        val tree = GameTreeModel.newTree()
        val newTree = tree.mutate { draft =>
          () // does nothing
        }
        assert(tree eq newTree)
      }
    }

    test("conversion to SGF NodeObject") {
      // TODO!

      val tree = GameTreeModel.newTree().mutate { draft =>
        draft.appendNode(0, "C" -> List("foo"))
      }

      val sgfSrc = GameTreeModel.exportToSgf(tree)
      val newTrees = Sgf.parse(sgfSrc)
      assert(newTrees.length == 1)
      val newTree = newTrees.head
      assert(newTree.children(0).data("C")(0) == "foo")
    }
  }
}
