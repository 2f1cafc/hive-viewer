package sabaki.sgf

import utest._
import scalajs.js

object SgfTests extends TestSuite {

  /* the smart assert macros in utest only print the value of local
   * vars
   */
  def assertEqual[A, B](left: => A, right: => B) = assert(left == right)

  val tests = Tests {

    test("parse") {
      test("parse an empty tree") {
        assert(Sgf.parse("()").length == 0)
      }

      test("parse a root node with default id") {
        val res = Sgf.parse("(;)")
        assert(res.length == 1)
        assert(res(0).id.asInstanceOf[Int] == 0)
      }

      test("acknowledge parse options") {
        val opts = new ParseOptions {
          override val getId = js.defined { () => 123 }
        }
        assert(Sgf.parse("(;)", opts)(0).id.asInstanceOf[Int] == 123)
      }

      test("parse basic properties in root node") {
        val res = Sgf.parse("""(;C[Saluton \] mondo])""")
        assert(res.length == 1)
        assertEqual(res(0).data("C").length, 1)
        assertEqual(res(0).data("C")(0), "Saluton ] mondo")
        assertEqual(res(0).children.length, 0)
      }

      test("parse multiple nodes") {
        val res = Sgf.parse("""(;C[begin];M[end])""")
        assertEqual(res.length, 1)
        assertEqual(res(0).children.length, 1)
      }

      test("stringify round trip") {
        val node = NodeObject(id = 10,
          data = js.Dictionary("C" -> js.Array("foo"))
        )
        val res = Sgf.parse(Sgf.stringify(js.Array(node)))
        assertEqual(res.length, 1)
        assertEqual(res(0).children.length, 0)
        assertEqual(res(0).data("C").length, 1)
        assertEqual(res(0).data("C")(0), "foo")
      }
    }
  }
}
