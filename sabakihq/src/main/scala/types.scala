package sabaki

import scalajs.js
import js._

object types {
  type Primitive = Int | Boolean | Char | String
  type NodeData = js.Dictionary[js.Array[Primitive]]

  object NodeData {
    def apply(data: (String, Primitive)*): NodeData = js.Dictionary(data.map { case (k,v) => k -> js.Array(v) }: _*)
    def pretty(data: NodeData): String =
      data.map { case (k, v) => s"$k->[$v]" }.mkString(" ;; ")
  }
}
