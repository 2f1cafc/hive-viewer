/*
 * see: https://github.com/SabakiHQ/immutable-gametree
 *  this is just a scalajs facade for that library
 */

package sabaki.gametreeFacade

import scalajs.js
import scalajs.js.annotation._

import sabaki.types._
import GameTree._
import sabaki.sgf.ParseOptions
import sabaki.sgf.Sgf

trait GameTreeOptions[Id] extends js.Object {
  val root: js.UndefOr[NodeObject[Id]] = js.undefined
  val getId: js.UndefOr[CbGetId[Id]] = js.undefined
  val merger: js.UndefOr[CbMerger] = js.undefined
}

object GameTreeOptions {
  def empty[Id] = new GameTreeOptions[Id] {}
  def apply[Id](root: NodeObject[Id]) = {
    val _root = root
    new GameTreeOptions[Id] {
      override val root = _root
    }
  }
}

@js.native
@JSImport("@sabaki/immutable-gametree", JSImport.Default)
class GameTree[Id](options: GameTreeOptions[Id] = GameTreeOptions.empty[Id]) extends js.Object {

  val root: NodeObject[Id] = js.native

  def mutate(mutator: CbMutator[Id]): GameTree[Id] = js.native
  def get(id: Id): NodeObject[Id] = js.native

  def onCurrentLine(id: Id, currents: js.Dictionary[Id]): Boolean = js.native

  def getId(): Id = js.native
  def getHash(): String = js.native
  def getStructureHash(): String = js.native
  def navigate(id: Id, step: Int, currents: js.Dictionary[Id]): NodeObject[Id] = js.native

  def getLevel(id: Id): Int = js.native

  // these functions are 'Generators' in JS land
  //   https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function*
  def listNodes(): js.Iterator[NodeObject[Id]] = js.native
  def getSequence(id: Id): js.Iterator[NodeObject[Id]] = js.native
  def listCurrentNodes(currents: js.Dictionary[Id] = js.Dictionary.empty): js.Iterator[NodeObject[Id]] = js.native
}

object GameTree {
  /* "The mutator will be called with a Draft class. In the mutator
   * function you will apply all your changes to the draft. Returns a
   * new GameTree instance with the changes you applied to the draft,
   * without changing the original GameTree instance."
   */
  type CbMutator[Id] = js.Function1[Draft[Id], Unit]
  type CbGetId[Id] = js.Function0[Id]
  type CbMerger = js.Function2[NodeObject[_], js.Object, js.Object] // may also return null


  def idCounter(from: Int): scalajs.js.Function0[Int] = {
    var count = Iterator.from(from)
    (() => count.next)
  }


  private val globalGetId = idCounter(0)

  def fromRootNode(node: NodeObject[Int]) = {
    // find the highest id, so we can create a counter that won't clash
    val tmpTree = new GameTree(
      new GameTreeOptions[Int] {
        override val root = node
      }
    )

    val highestId =
      tmpTree.listNodes().toIterator.foldLeft(0) { (acc, node) =>
        Math.max(acc, node.id)
      }

    new GameTree(
      new GameTreeOptions[Int] {
        override val root = node
        override val getId = idCounter(highestId + 1)
      }
    )
  }

  // makes sure the parse and the game tree have the same id
  // generator, using Id = Int
  def fromSgf(contents: String) = {
    val nodes = Sgf.parse[Int](contents,
      new ParseOptions[Int] { override val getId = globalGetId }
    )
    new GameTree(
      new GameTreeOptions[Int] {
        override val root = nodes(0)
        override val getId = globalGetId
      }
    )
  }

  implicit class GameTreeRich[Id](tree: GameTree[Id]) {
    def lastNode(currents: js.Dictionary[Id]): NodeObject[Id] = tree.listCurrentNodes(currents).toIterator.toSeq.last

    def getOpt(nodeId: Id): Option[NodeObject[Id]] = Option(tree.get(nodeId))
  }

}

// trait AppendNodeOptions extends js.Object {
//   val
// }

@js.native
trait Draft[Id] extends js.Object {
  val root: NodeObject[Id] = js.native
  def appendNode(
    parentId: Id,
    data: NodeData,
    options: js.UndefOr[js.Object] = ()
  ): Id = js.native
  def addToProperty(id: Id, property: String, value: js.Any): Boolean = js.native
  def updateProperty(id: Id, property: String, value: js.Any): Boolean = js.native
  def removeProperty(id: Id, property: String): Boolean = js.native
  def removeNode(id: Id): Boolean = js.native
}

// {
//   id: <Primitive>,
//   data: {
//     [property: <String>]: <Array<Primitive>>
//   },
//   parentId: <Primitive> | null,
//   children: <Array<NodeObject>>
// }
@js.native
trait NodeObject[Id] extends js.Object {
  val id: Id = js.native
  val data: NodeData = js.native
  val parentId: Id = js.native
  val children: js.Array[NodeObject[Id]] = js.native
}

object NodeObject {
  // enhances the basic the javascript node with some useful typed
  // scala utils
  implicit class RichNodeObject(node: NodeObject[_]) {
    // assumes only one prop assigned to this key and returns (or none)
    def prop1[T](key: String): Option[T] =
      node.data.get(key).flatMap(_.headOption.map(_.asInstanceOf[T]))
  }
}
