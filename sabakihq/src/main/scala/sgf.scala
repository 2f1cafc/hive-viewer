package sabaki.sgf

/* scalajs wrapper around https://github.com/SabakiHQ/sgf */

import scalajs.js
import scalajs.js.annotation.JSImport

/* the NodeObject from this library is interchangable with that from
 * the sgf lib (by design). */
import sabaki.gametreeFacade.NodeObject
import sabaki.gametreeFacade.GameTree
import sabaki.gametreeFacade.GameTreeOptions

// {
//   id: <Primitive>,
//   data: {
//     [property: <String>]: <Array<Primitive>>
//   },
//   parentId: <Primitive> | null,
//   children: <Array<NodeObject>>
// }

// trait NodeObject extends js.Object {
//   val id: js.Any
//   val data: js.Dictionary[js.Array[String]]
//   val parentId: js.Any
//   val children: js.Array[NodeObject]
// }

// object NodeObject {
//   def apply(
//     id: js.Any,
//     data: js.Dictionary[js.Array[String]] = js.Dictionary(),
//     parentId: js.Any = null,
//     children: js.Array[NodeObject] = js.Array()
//   ) = {
//     val (_id, _data, _parentId, _children) = (id, data, parentId, children)
//     new NodeObject {
//       val id = _id
//       val data = _data
//       val parentId = _parentId
//       val children = _children
//     }
//   }
// }


trait ParseOptions[Id] extends js.Object {
  import js.`|`
  val getId: js.UndefOr[js.Function0[Id]] = js.undefined
}

@js.native
@JSImport("@sabaki/sgf", JSImport.Default)
object Sgf extends js.Object {
  def parse[Id](contents: String, options: js.UndefOr[ParseOptions[Id]] = js.undefined): js.Array[NodeObject[Id]] = js.native

  def stringify(nodes: js.Array[NodeObject[_]]): String = js.native
}
