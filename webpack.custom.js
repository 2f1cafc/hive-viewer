var path = require('path');

/* need to, for example, exclude fs from sgf so it can run in the
 * browser (where those functions don't exist */

var webpack = require('webpack');

module.exports = require('./scalajs.webpack.config');

// see https://github.com/SabakiHQ/sgf/blob/master/README.md
module.exports.externals = {
    'fs': 'null',
    'jschardet': 'null',
    'iconv-lite': 'null'
};

module.exports.devServer = {
    contentBase: path.join(__dirname, 'static')
};
